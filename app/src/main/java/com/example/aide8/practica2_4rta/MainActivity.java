package com.example.aide8.practica2_4rta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button este, estas2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        este=(Button)findViewById(R.id.button);

        estas2=(Button)findViewById(R.id.button2);

        este.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent botonLogin = new Intent(MainActivity.this, ActividadPasarParametro.class);
                startActivity(botonLogin);// estaba en una mision importante


            }
        });
        estas2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btn = new Intent(MainActivity.this, ActividadRecibirParametro.class);
                startActivity(btn);

            }
        });

    }
}
