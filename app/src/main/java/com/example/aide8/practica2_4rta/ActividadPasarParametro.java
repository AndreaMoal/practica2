package com.example.aide8.practica2_4rta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActividadPasarParametro extends AppCompatActivity {

        EditText cajaDatos;
        Button botonEnviar;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_actividad_recibir_parametro);
            cajaDatos = (EditText) findViewById(R.id.editText);
            botonEnviar = (Button) findViewById(R.id.button3);

            // este es el metdod para el llamar al boton

            botonEnviar.setOnClickListener(new View.OnClickListener() {// para que puedas poner tdo este cosigo haces esto
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActividadPasarParametro.this,ActividadRecibirParametro.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("dato",cajaDatos.getText().toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });


        }
}
